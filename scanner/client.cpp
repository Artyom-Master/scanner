#include "client.h"
#include <QTcpSocket>
#include <QDataStream>
#include <QHostAddress>
#include <QFile>
#include <QTextCodec>
#include <QDebug>


Client::Client(QObject *parent)
    : QObject(parent)
    , m_clientSocket(new QTcpSocket(this))
{
    connect(m_clientSocket, &QTcpSocket::connected, this, &Client::initDevice);
    connect(m_clientSocket, &QTcpSocket::connected, this, &Client::connected);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, &Client::disconnected);
    connect(m_clientSocket, &QTcpSocket::readyRead, this, &Client::onReadyRead);
    //connect(m_clientSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &ChatClient::error);
}

void Client::initDevice()
{
    if(!check) sendMessage("initialization");
    if(check) Q_EMIT readyForMeasurement();
}

void Client::sendMessage(const QString &text)
{
    if (text.isEmpty())
        return;
    QByteArray w;
    w.resize(8);
    if(text == "initialization")
       {
        w[0] = 2;
        w[1] = 2;
        w[2] = 0;
        w[3] = 0;
        w[4] = 0;
        w[5] = 0;
        w[6] = 0;
        w[7] = 0;
    } else if(text == "measure")
    {
        w[0] = 2;
        w[1] = 1;
        w[2] = 0;
        w[3] = 0;
        w[4] = 0;
        w[5] = 0;
        w[6] = 0;
        w[7] = 0;
        pointsArrived = true;
    } /*else if(text == "set resolution")
    {
        w.resize(3);
        w[0] = 12;
        w[1] = 11;
        w[2] = 0.5f;
    }*/ else if(text == "exit")
    {
        w[0] = 2;
        w[1] = 3;
        w[2] = 0;
        w[3] = 0;
        w[4] = 0;
        w[5] = 0;
        w[6] = 0;
        w[7] = 0;
    }
    m_clientSocket->write(w);
}

void Client::disconnectFromDevice()
{
    m_clientSocket->disconnectFromHost();
}

void Client::connectToDevice(const QHostAddress &address, quint16 port)
{
    m_clientSocket->connectToHost(address, port);
}

void Client::onReadyRead()
{
    if(!pointsArrived)
    {
       QByteArray buffer;
       QString message;
       if(m_clientSocket->bytesAvailable()) buffer = m_clientSocket->readAll();
       if(buffer.size() == 4 && buffer.at(0) == 3)
       {
            check = true;
            message = "Device launched";
            Q_EMIT messageReceived(&message);
            disconnectFromDevice();
            connectToDevice(QHostAddress(hostAddress), 50150);
            return;
       }
    }
    QByteArray data, buffer;
    float *point;
    quint32 length = 256*256*3*4;
    //QString error = "timed out";
    QStringList points;
    //float numberList[length/4];
    /*QFile file("C:/Users/arthi/OneDrive/Documents/ChatExample-master/TcpChatUser2/image.txt");
    file.open(QFile::ReadWrite | QFile::Text);*/
    while(length > 0)
    {
        if(!m_clientSocket->waitForReadyRead())
        {
            //Q_EMIT messageReceived(&error);
            return;
        }
        buffer = m_clientSocket->read(length);
        data += buffer;
        length -= buffer.size();
        //forFile = QTextCodec::codecForMib(106)->toUnicode(data);
        /*data.split('\0');
        Q_FOREACH(QString line, points)
        {

        }*/
        //file.write(data);
    }
    //numberList = (float *)data.data();
    /*QByteArray list;
    for(int i = 0; i < 196608*4; i+=4)
    {
        if(i + 3 > 196608*4) break;
        list += (data.at(i) + data.at(i + 1) + data.at(i + 2) + data.at(i + 3));
    }
    quint32 lenght = list.size();
    numberList = (float *)list.data();*/
    /*forFile.resize(256*256*3*4);
    int some = 0;
    forFile = file.readAll();
    some = forFile.size();
    file.close();*/
    /*for(int i = 0; i < data.size(); i++)
    {
        if(data.at(i) != 0)
            qDebug() << data.at(i);
    }*/
    for(int i = 0, j = 0; i < data.size(); i+=4, j++)
    {
       if(i + 4 >= data.size()) break;
       numberList[j] = castToFloat(data.at(i), data.at(i + 1), data.at(i + 2), data.at(i + 3));
       //real_length++;
    }
/*
    for(int i = 0; i < 196608; i++)
    {
        if(numberList[i] != 0)
        qDebug() << numberList[i] << "   ";
    }
*/
    Q_EMIT drawPoints();
    readyForMeasurement();
    /*for(quint32 i = 0; i < real_length; i++)
    {
        message += QString::number(numberList[i]);
    }
    Q_EMIT messageReceived(&message);*/
}

float Client::castToFloat(char byte_1, char byte_2, char byte_3, char byte_4)
{
    if(byte_1 != 0 && byte_2 != 0 && byte_3 != 0 && byte_4 != 0)
        qt_noop();
    float number = byte_1 << 24 | byte_2 << 16 | byte_3 << 8 | byte_4;
    return number;
    //return *reinterpret_cast<float*>(&number);
}
