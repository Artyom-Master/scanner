#include "viewer.h"

Viewer::Viewer(QWidget *parent) : QGLViewer(parent)
{

}

void Viewer::drawFigure()
{
    index = 1;
    update();
}

void Viewer::clear()
{
    index = 2;
    update();
}

void Viewer::draw()
{
    if(index == 0)
    {
       /* drawGrid(1.0, 10);
        glBegin(GL_LINES);
        glColor3f(1, 1, 1);
        glVertex3f(1, 0, 0);
        glVertex3f(0, 1, 0);
        glVertex3f(0, 0, 1);
        glEnd();*/
    } else if(index == 1 && pointCloud != nullptr)
    {
        glBegin(GL_POINTS);
        for(int i = 0; i < 196608; i+=3)
        {
            glVertex3f(*(pointCloud + i), *(pointCloud + i + 1), *(pointCloud + i + 2));
        }
        glEnd();
    }
}
