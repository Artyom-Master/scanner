#include "dialog.h"
#include "viewer.h"
#include "client.h"
#include "ui_dialog.h"
#include <QInputDialog>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
    , m_client(new Client(this))
{
    ui->setupUi(this);
    ui->measurementButton->setEnabled(false);
    ui->setResolutionButton->setEnabled(false);
    connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(attemptConnection()));
    connect(ui->measurementButton, SIGNAL(clicked()), this, SLOT(takeMeasurement()));
    connect(ui->exitButton, SIGNAL(clicked()), this, SLOT(closeAPP()));
    connect(m_client, SIGNAL(drawPoints()), this, SLOT(drawFigure()));
    connect(m_client, SIGNAL(connected()), this, SLOT(connectedToDevice()));
    connect(m_client, SIGNAL(disconnected()), this, SLOT(disconnectedFromDevice()));
    connect(m_client, SIGNAL(readyForMeasurement()), this, SLOT(enableMaesurement()));
    //connect(ui->pushButton_2, SIGNAL(clicked()), ui->viewer, SLOT(clear()));
    //connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(close()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::closeAPP()
{
    close();
}

void Dialog::attemptConnection()
{
    ui->connectButton->setEnabled(false);
    if(toConnect)
    {
        const QString hostAddress = QInputDialog::getText(
            this
            , tr("Chose Device")
            , tr("Device Address")
            , QLineEdit::Normal
            , QStringLiteral("192.168.2.33")
        );
        if (hostAddress.isEmpty())
            return;
        toConnect = false;
        m_client->hostAddress = hostAddress;
        ui->connectButton->setText(tr("Disconnect from Device"));
        m_client->connectToDevice(QHostAddress(hostAddress), 50150);
    }
    else
    {
        toConnect = true;
        ui->connectButton->setText(tr("Connect"));
        //m_client->sendMessage("Client disconnected");
        m_client->disconnectFromDevice();
    }
}

void Dialog::enableMaesurement()
{
    ui->measurementButton->setEnabled(true);
    //ui->setResolutionButton->setEnabled(true);
}

void Dialog::disconnectedFromDevice()
{
    if(toConnect) ui->connectButton->setEnabled(true);
}

void Dialog::connectedToDevice()
{
    if(!toConnect) ui->connectButton->setEnabled(true);
}

void Dialog::takeMeasurement()
{
    m_client->sendMessage("measure");
    ui->measurementButton->setEnabled(false);
    //ui->setResolutionButton->setEnabled(false);
}

void Dialog::drawFigure()
{
    ui->viewer->pointCloud = m_client->numberList;
    ui->viewer->drawFigure();
}

/*void Dialog::drawFigure()
{
    Q_EMIT drawSomething();
    QFile file("C:/Users/arthi/OneDrive/Documents/untitled3/Paraboloid.txt");
    if(!file.open(QFile::ReadOnly | QFile::Text)) return;
    QString pointss = file.readAll();
    if(pointss == nullptr) return;
    ui->viewer->setPointsCloud(&pointss);
    file.close();
    ui->viewer->repaint();
}*/
