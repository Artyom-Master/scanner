#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
class QHostAddress;
class Client : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Client)
public:
    explicit Client(QObject *parent = nullptr);
    QString hostAddress;
    float numberList[196608];
public Q_SLOTS:
    void connectToDevice(const QHostAddress &address, quint16 port);
    void sendMessage(const QString &text);
    void disconnectFromDevice();
    void initDevice();
private Q_SLOTS:
    void onReadyRead();
Q_SIGNALS:
    void connected();
    void disconnected();
    void messageReceived(QString *text);
    void drawPoints();
    void readyForMeasurement();
    //void error(QAbstractSocket::SocketError socketError);
private:
    QTcpSocket *m_clientSocket;
    bool check = false;
    bool pointsArrived = false;
    float castToFloat(char byte_1, char byte_2, char byte_3, char byte_4);
};

#endif // CLIENT_H
