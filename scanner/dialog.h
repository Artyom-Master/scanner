#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Client;
class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();
private:
    Ui::Dialog *ui;
    Client *m_client;
    bool toConnect = true;
private Q_SLOTS:
    //void drawFigure();
    void attemptConnection();
    void connectedToDevice();
    void enableMaesurement();
    void disconnectedFromDevice();
    void closeAPP();
    void takeMeasurement();
    void drawFigure();
/*Q_SIGNALS:
    void drawSomething();*/
};
#endif // DIALOG_H
